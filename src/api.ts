import { LOGIN_URL, CRED_URL, LOGOUT_URL } from "./urls";
import CryptoJS from 'crypto-js'

export interface Credential {
  id: number;
  url: string;
  name: string; // Used when url is empty
  email: string;
  username: string;
  password: string;
  description: string;
  salt: string; // Random string
};

export interface User {
  id: number;
  username: string;
  hash: string;
  role: string;
  credentialMaxId: number;
};

interface SpringErrorResponse {
  timestamp: Date,
  status: number,
  error: string,
  message: string,
  path: string
};

export class HttpError extends Error {
  status: number;

  constructor(status: number, message: string) {
    super(message);

    this.status = status;
  }
};

const cred = {
  getAll: async (userId: number, pin: string): Promise<Credential[]> => {
    const headers = new Headers();
    headers.set('Accept', 'application/json, */*;q=0.8');

    const response = await fetch(CRED_URL(userId) + '/', {
      method: 'GET',
      credentials: 'include',
      headers
    }).catch(err => {throw new HttpError(0, err instanceof Error ? err.message : err as any);});

    if (!response.ok) {
      if (response.status === 401) {
        throw new HttpError(401, 'Invalid credentials');
      }
      const data: SpringErrorResponse = await response.json();
      throw new HttpError(response.status, `${data.error}: ${data.message}`);
    }

    const data: {id: number, userId: number, content: string}[] = await response.json();
    const credentials = data.map(item => {
      const decrypted = CryptoJS.AES.decrypt(item.content, pin).toString(CryptoJS.enc.Utf8);
      const credential: Credential = JSON.parse(decrypted);
      credential.id = item.id;
      return credential;
    });
    return credentials;
  },
  postCred: async (userId: number, cred: Credential, pin: string): Promise<Credential> => {
    const headers = new Headers();
    headers.set('Accept', 'application/json, */*;q=0.8');
    headers.set('Content-Type', 'application/json;charset=UTF-8');

    cred.id = 0;
    const requestBody = {
      content: CryptoJS.AES.encrypt(JSON.stringify(cred), pin).toString()
    };
    const response = await fetch(CRED_URL(userId) + '/', {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify(requestBody),
      headers
    }).catch(err => {throw new HttpError(0, err instanceof Error ? err.message : err as any);});

    if (!response.ok) {
      if (response.status === 401) {
        throw new HttpError(401, 'Invalid credentials');
      }
      const data: SpringErrorResponse = await response.json();
      throw new HttpError(response.status, `${data.error}: ${data.message}`);
    }

    const responseBody: {id: number, userId: number, content: string} = await response.json();
    const decrypted = CryptoJS.AES.decrypt(responseBody.content, pin).toString(CryptoJS.enc.Utf8);
    const responseCred: Credential = JSON.parse(decrypted);
    responseCred.id = responseBody.id;
    return responseCred;
  },
  deleteCred: async (userId: number, credId: number): Promise<void> => {
    const headers = new Headers();

    const response = await fetch(CRED_URL(userId) + '/' + credId, {
      method: 'DELETE',
      credentials: 'include',
      headers
    }).catch(err => {throw new HttpError(0, err instanceof Error ? err.message : err as any);});

    if (!response.ok) {
      if (response.status === 401) {
        throw new HttpError(401, 'Invalid credentials');
      }
      const data: SpringErrorResponse = await response.json();
      throw new HttpError(response.status, `${data.error}: ${data.message}`);
    }

    // Return nothing
    return;
  },
  putCred: async (userId: number, cred: Credential, pin: string): Promise<Credential> => {
    const headers = new Headers();
    headers.set('Accept', 'application/json, */*;q=0.8');
    headers.set('Content-Type', 'application/json;charset=UTF-8');

    const credId = cred.id;
    cred.id = 0;
    const requestBody = {
      content: CryptoJS.AES.encrypt(JSON.stringify(cred), pin).toString()
    };
    const response = await fetch(CRED_URL(userId) + '/' + credId, {
      method: 'PUT',
      credentials: 'include',
      body: JSON.stringify(requestBody),
      headers
    }).catch(err => {throw new HttpError(0, err instanceof Error ? err.message : err as any);});

    if (!response.ok) {
      if (response.status === 401) {
        throw new HttpError(401, 'Invalid credentials');
      }
      const data: SpringErrorResponse = await response.json();
      throw new HttpError(response.status, `${data.error}: ${data.message}`);
    }

    const responseBody: {id: number, userId: number, content: string} = await response.json();
    const decrypted = CryptoJS.AES.decrypt(responseBody.content, pin).toString(CryptoJS.enc.Utf8);
    const responseCred: Credential = JSON.parse(decrypted);
    responseCred.id = responseBody.id;
    return responseCred;
  }
};

const auth = {
  authenticate: async (username: string, password: string): Promise<User> => {
    const headers = new Headers();
    headers.set('Accept', 'application/json, */*;q=0.8');
    headers.set('Authorization', 'Basic ' + btoa(username + ":" + password));

    const response = await fetch(LOGIN_URL(), {
      method: 'GET',
      credentials: 'include',
      headers
    }).catch(err => {throw new HttpError(0, err instanceof Error ? err.message : err as any);});

    if (!response.ok) {
      if (response.status === 401) {
        throw new HttpError(401, 'Invalid credentials');
      }
      const data: SpringErrorResponse = await response.json();
      throw new HttpError(response.status, `${data.error}: ${data.message}`);
    }

    return await response.json();
  },
  logout: async (): Promise<void> => {
    const headers = new Headers();

    const response = await fetch(LOGOUT_URL(), {
      method: 'POST',
      credentials: 'include',
      headers
    }).catch(err => {throw new HttpError(0, err instanceof Error ? err.message : err as any);});

    if (!response.ok) {
      if (response.status === 401) {
        throw new HttpError(401, 'Invalid credentials');
      }
      const data: SpringErrorResponse = await response.json();
      throw new HttpError(response.status, `${data.error}: ${data.message}`);
    }

    // Return nothing
    return;
  }
};

export const api = { cred, auth };
