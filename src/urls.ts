export const BASE_URL = () => 'https://pm.0chaos.eu/v4';
export const LOGIN_URL = () => BASE_URL() + '/authenticate';
export const LOGOUT_URL = () => BASE_URL() + '/logout';
export const USER_URL = () => BASE_URL() + '/user';
export const CRED_URL = (userId: number) => USER_URL() + '/' + userId + '/credential';
