import { FC, useMemo, useState } from 'react';
import { useAuth } from '../components/AuthContext';
import { api } from '../api'
import { Store } from 'react-notifications-component';
import { Credential } from '../api';
import { focusManager, useMutation, useQuery, useQueryClient } from 'react-query';
import { CredentialList } from '../components/CredentialList';
import { CredentialItem } from '../components/CredentialItem';
import { UploadDialog } from '../components/UploadDialog';

export const CredentialsPage: FC<{}> = (props) => {
  const { userId, pin, logout } = useAuth();
  const [selectedId, setSelectedId] = useState<number | null>(null);
  const [isEditing, setIsEditing] = useState(false);
  const [isUploading, setIsUploading] = useState(false);
  const [searchQuery, setSearchQuery] = useState('');

  const getCredentials = async () => {
    return await api.cred.getAll(userId, pin);
  };
  const addCredential = async (cred: Credential) => {
    return await api.cred.postCred(userId, cred, pin);
  };
  const updateCredential = async (cred: Credential) => {
    return await api.cred.putCred(userId, cred, pin);
  }
  const deleteCredential = async (credId: number) => {
    return await api.cred.deleteCred(userId, credId);
  }
  // const updateCredential = async ()
  const generateSalt = (length: number) => {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < length; i += 1) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
  };

  const queryClient = useQueryClient();
  const { data: allCredentials, status, error } = useQuery(['credentials'], getCredentials);
  const { mutateAsync: addMutate } = useMutation(addCredential, {
    onSuccess: () => {
      queryClient.invalidateQueries(['credentials']);
    }
  });
  const { mutateAsync: addMutateNoInvalidation } = useMutation(addCredential, {});
  const { mutateAsync: updateMutate } = useMutation(updateCredential, {
    onMutate: async (cred) => {
      await queryClient.cancelQueries(['credentials', cred.id]);
      const previousCredentials: Credential[] = queryClient.getQueryData('credentials') as any;
      queryClient.setQueryData(['credentials'], [...previousCredentials.filter(c => c.id !== cred.id), cred]);
      return { previousCredentials, cred };
    },
    onError: async (err, cred, context: any) => {
      queryClient.setQueryData(['credentials'], context.previousCredentials);
    },
    onSettled: cred => {
      queryClient.invalidateQueries(['credentials']);
    }
  });
  const { mutateAsync: deleteMutate } = useMutation(deleteCredential, {
    onSuccess: (cred) => {
      queryClient.invalidateQueries(['credentials']);
    }
  })
  const selectedCredential: Credential | null = useMemo(() => {
    if (selectedId === null || allCredentials == null) return null;
    const filtered = allCredentials.filter(cred => cred.id === selectedId);
    if (filtered.length < 1) return null;
    return filtered[0];
  }, [allCredentials, selectedId]);

  const onAdd = () => {
    addMutate({
      id: 0,
      url: '',
      name: 'Unnamed credential',
      email: '',
      username: '',
      password: '',
      description: '',
      salt: generateSalt(16)
    })
      .then((cred) => {
        setSelectedId(cred.id);
      });
  };
  const onEdit = (cred: Credential) => {
    updateMutate(cred);
  };
  const onDelete = (credId: number) => {
    if (window.confirm('Delete credential?')) {
      deleteMutate(credId);
    }
  };
  const onDownload = () => {
    if (status !== 'success') {
      // Only download if data is available
      Store.addNotification({
        title: 'Cannot download credentials',
        type: 'warning',
        container: 'top-right',
        dismiss: {
          duration: 2000
        }
      });
      return;
    }
    const stringifyToText = (credentials: Credential[]): string =>
      credentials.map(cred =>
        `${cred.name !== '' ? `${cred.name} - ${cred.url}` : cred.url}\n` +
        `${cred.email !== '' ? `Email: ${cred.email}\n` : ''}` +
        `${cred.username !== '' ? `Username: ${cred.username}\n` : ''}` +
        `${cred.password !== '' ? `Password: ${cred.password}\n` : ''}` +
        `${cred.description !== '' ? `Descripion: ${cred.description}\n` : ''}`
      ).join('\n');
    const downloadText = (text: string, mime: string, name: string): void => {
      const elm = document.createElement('a');
      elm.href = `data:${mime};charset=utf-8,${encodeURIComponent(text)}`;
      elm.setAttribute('download', name);
      elm.style.display = 'none';
      document.body.appendChild(elm);
      elm.click();
      document.body.removeChild(elm);
    };
    downloadText(stringifyToText(allCredentials), 'text/plain', 'accounts.txt');
    downloadText(JSON.stringify(allCredentials), 'application/json', 'accounts.json');
  };
  const onUpload = () => {
    if (status !== 'success') {
      // Only upload if data is available
      Store.addNotification({
        title: 'Cannot download credentials',
        type: 'warning',
        container: 'top-right',
        dismiss: {
          duration: 2000
        }
      });
      return;
    }

    focusManager.setFocused(false);
    setIsUploading(true);
  }
  const onFileCancel = () => {
    setIsUploading(false);
    focusManager.setFocused(undefined);
    console.log('cancel');
  };
  const onFileUpload = async (text: string) => {
    setIsUploading(false);
    try {
      const credentials: Credential[] = JSON.parse(text);
      // Check integrity of all credentials before starting
      for (const cred of credentials) {
        try {
          if (cred.url == null || cred.name == null || cred.email == null ||
            cred.username == null || cred.password == null ||
            cred.description == null) {
            throw Error("Invalid credential");
          }
        } catch {
          Store.addNotification({
            title: 'Encountered invalid credential during upload',
            type: 'warning',
            container: 'top-right',
            dismiss: {
              duration: 2000
            }
          });
          return;
        }
      }
      // Upload each sequentially
      let i = 0;
      for (const cred of credentials) {
        await addMutateNoInvalidation({
          id: 0,
          url: cred.url,
          name: cred.name,
          email: cred.email,
          username: cred.username,
          password: cred.password,
          description: cred.description,
          salt: generateSalt(16)
        });
        i += 1;
        if (i % 2 == 0) {
          Store.addNotification({
            title: `Upload: ${i}/${credentials.length}`,
            type: 'default',
            container: 'top-right',
            dismiss: {
              duration: 200
            }
          });
        }
      }
    } finally {
      queryClient.invalidateQueries(['credentials']);
      focusManager.setFocused(undefined);
    }
  };
  const onLogout = () => {
    logout();
  };
  const onIsEditing = (isEditing: boolean) => {
    if (isEditing) {
      focusManager.setFocused(false);
    } else {
      focusManager.setFocused(undefined);
    }
    setIsEditing(isEditing);
  };

  return (
    <div className="credentialsPage h-100">
      {isUploading ? <UploadDialog onCancel={onFileCancel} onUpload={onFileUpload} /> : <></>}
      <div className="row h-100 g-0">
        <div className="h-sm-100 col-sm-4 col-md-4 col-lg-3 col-xl-3 col-xxl-2 pe-0">
          <div className="leftPanel h-sm-100 d-flex flex-column">
            {status === 'error' && (
              <div style={{ padding: '9px', color: 'white' }}>
                <span>Error</span>
              </div>
            )}
            {status === 'loading' && (
              <div style={{ padding: '9px', color: 'white' }}>
                <span>Loading...</span>
              </div>
            )}
            {status === 'success' && (
              <>
                <div style={{ display: 'contents' }}>
                  <div style={{ padding: '9px', color: 'white' }}>
                    <input
                      type="text"
                      className="w-100"
                      value={searchQuery}
                      onChange={e => setSearchQuery(e.currentTarget.value)}
                    />
                  </div>
                  <CredentialList
                    className="mb-0 ps-0 flex-grow-1"
                    searchQuery={searchQuery}
                    credentials={allCredentials}
                    selectedId={selectedId}
                    onSelect={setSelectedId}
                  />
                </div>
                <div style={{ padding: '9px', color: 'white' }}>
                  <span
                    className="pe-2"
                    style={{ color: '#2add89' }}
                    onClick={onAdd}
                  >Add</span>
                  <span
                    className="pe-2"
                    style={{ color: '#00c3ff' }}
                    onClick={onDownload}
                  >Download</span>
                  <span
                    className="pe-2"
                    style={{ color: '#00c3ff' }}
                    onClick={onUpload}
                  >Upload</span>
                  <span
                    className=""
                    style={{ color: '#ffc107' }}
                    onClick={onLogout}
                  >Logout</span>
                </div>
              </>
            )}
          </div>
        </div>
        <div className="rightPanel col h-xs-100 h-sm-100 p-2">
          {selectedCredential === null
            ? <p>...</p>
            : <CredentialItem
              cred={selectedCredential}
              onEdit={onEdit}
              onDelete={onDelete}
              onEditing={onIsEditing}
            />
          }
        </div>
      </div>
    </div>
  );
};
