import { FC, FormEvent, useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { Store } from 'react-notifications-component';
import { HttpError } from "../api";
import { useAuth } from "../components/AuthContext";

export const LoginPage: FC<{}> = (props) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [pin, setPin] = useState('');
  const [rememberMe, setRememberMe] = useState(false);
  const {login, isDoneLoadingFromStorage, getLoginInfo} = useAuth();

  useEffect(() => {
    (async () => {
      const {username, password, pin, saveLogin: rememberMe} = await getLoginInfo();
      setUsername(username);
      setPassword(password);
      setPin(pin);
      setRememberMe(rememberMe);
    })();
  }, [isDoneLoadingFromStorage]);

  const handleError = (err: any) => {
    let title: string = 'Error';
    let message: string = 'Unknown error';

    if (err instanceof HttpError) {
      title = `HTTP Error ${err.status}`;
      message = err.message;
    } else if (err instanceof Error) {
      message = err.message;
    } else {
      message = err;
    }

    Store.addNotification({
      title: title,
      message: message,
      type: 'danger',
      container: 'top-right',
      dismiss: {
        duration: 10000
      }
    });
  };

  const submit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    login(username, password, pin, rememberMe)
      .catch(handleError);
  };

  return (
    <div className="login">
      <p style={{ visibility: 'hidden' }}>.</p>
      <div className="d-flex justify-content-center align-items-center" style={{ marginTop: '20vh' }}>
        <Form className="rounded p-4 p-sm-3" onSubmit={submit}>
          <Form.Group className="mb-3" controlId="formBasicUsername">
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              value={username}
              onInput={e => setUsername(e.currentTarget.value)}
              placeholder="Enter Username"
              minLength={3}
              maxLength={100}
              required
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              value={password}
              onInput={e => setPassword(e.currentTarget.value)}
              placeholder="Enter Password"
              minLength={3}
              maxLength={100}
              required
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPin">
            <Form.Label>Pin</Form.Label>
            <Form.Control
              type="password"
              value={pin}
              onInput={e => setPin(e.currentTarget.value)}
              placeholder="Enter PIN"
              minLength={3}
              maxLength={100}
              required
            />
            <Form.Text className="text-muted">
              The PIN is used as the encryption key. Please use a long and <br/>
              secure PIN. The PIN is never sent to the server, but may <br/>
              be stored in LocalStorage.
            </Form.Text>
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicCheckbox">
            <Form.Check
              type="checkbox"
              checked={rememberMe}
              onChange={e => setRememberMe(e.currentTarget.checked)}
              label="Remember Me"
            />
          </Form.Group>
          <Button variant="primary" type="submit">Login</Button>
        </Form>
      </div>
    </div>
  )
}
