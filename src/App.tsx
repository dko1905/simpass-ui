import { FC } from 'react';
import { LoginPage } from './pages/LoginPage';
import { useAuth } from './components/AuthContext';
import { QueryClient, QueryClientProvider } from 'react-query';
import { CredentialsPage } from './pages/CredentialsPage';
import { ReactQueryDevtools } from 'react-query/devtools';

const queryClient = new QueryClient();

export const App: FC = () => {
  const { isLoading, isLoggedIn } = useAuth();

  return (
    <main className="App">
      <QueryClientProvider client={queryClient}>
        {isLoading
          ? <p>Loading...</p>
          : !isLoggedIn
            ? <LoginPage />
            : <CredentialsPage />
        }
        <ReactQueryDevtools />
      </QueryClientProvider>
    </main>
  );
}
