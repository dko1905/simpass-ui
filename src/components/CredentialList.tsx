import { FC, useMemo, useState } from "react";
import { useMutation, useQuery } from "react-query";
import { Credential } from '../api';

interface Props {
  credentials: Credential[]
  selectedId: number | null,
  searchQuery: string,
  onSelect: (selectedId: number) => void,
  className?: string,
};

export const CredentialList: FC<Props> = (props) => {
  const filteredCredentials = useMemo(() => {
    if (props.searchQuery.trim() === '') {
      return props.credentials
        .sort((a, b) => a.url > b.url ? 1 : - 1)
        .sort((a, b) => a.name > b.name ? 1 : -1);
    }

    // Case-insensitive .includes()
    const includesIn = (a: string, b: string) => {
      return a.toLowerCase().includes(b.toLowerCase());
    };

    const arr = [
      ...props.credentials.filter(cred => includesIn(cred.name, props.searchQuery)),
      ...props.credentials.filter(cred => includesIn(cred.url, props.searchQuery)),
      ...props.credentials.filter(cred => includesIn(cred.description, props.searchQuery)),
      ...props.credentials.filter(cred =>
        includesIn(cred.email, props.searchQuery)
        || includesIn(cred.password, props.searchQuery)
        || includesIn(cred.username, props.searchQuery)
      )
    ];
    return arr.filter((cred, index) =>
      arr.indexOf(cred) === index
    );
  }, [props.credentials, props.searchQuery])

  return (
    <ul className={"leftList " + props.className}>
      {props.credentials.length === 0
        ? <li><p>No credentials</p></li>
        : filteredCredentials.map(cred =>
          <li
            key={cred.id}
            className={props.selectedId === cred.id ? 'selected' : ''}
            onClick={e => props.onSelect(cred.id)}
          >
            {cred.url !== '' ? cred.url : cred.name}
          </li>
        )
      }
    </ul>
  );
};
