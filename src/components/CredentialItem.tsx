import { FC, FormEvent, MouseEvent, useEffect, useMemo, useState } from "react";
import { Button, ButtonGroup, Form } from "react-bootstrap";
import { Store } from "react-notifications-component";
import { Credential } from "../api";

interface Props {
  cred: Credential,
  onEditing: (editing: boolean) => void,
  onEdit: (cred: Credential) => void,
  onDelete: (credId: number) => void
};

export const CredentialItem: FC<Props> = (props) => {
  const cred = useMemo(() => props.cred, [props.cred]);
  const [isEditing, setIsEditing] = useState(false);
  const [url, setUrl] = useState(cred.url);
  const [name, setName] = useState(cred.name);
  const [email, setEmail] = useState(cred.email);
  const [username, setUsername] = useState(cred.username);
  const [password, setPassword] = useState(cred.password);
  const [description, setDescription] = useState(cred.description);

  useEffect(() => {
    props.onEditing(false);
    setIsEditing(false);
    setUrl(cred.url);
    setName(cred.name);
    setEmail(cred.email);
    setUsername(cred.username);
    setPassword(cred.password);
    setDescription(cred.description);
  }, [cred]);

  const edit = () => {
    setIsEditing(true);
    props.onEditing(true);
  };

  const save = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setIsEditing(false);
    props.onEditing(false);
    props.onEdit({
      id: cred.id,
      url,
      name,
      email,
      username,
      password,
      description,
      salt: cred.salt
    });
  };

  const cancel = () => {
    setIsEditing(false);
    props.onEditing(false);
  };

  const deleteCred = () => {
    setIsEditing(false);
    props.onEditing(false);
    props.onDelete(cred.id);
  };

  const copyToClip = (e: MouseEvent<HTMLDivElement>, text: string) => {
    navigator.clipboard.writeText(text);
    Store.addNotification({
      title: `Copied "${text}" to clipboard`,
      type: 'success',
      container: 'top-right',
      dismiss: {
        duration: 1000
      }
    });
  };

  return (
    <>
      {!isEditing
        ? <>
          {name !== ''
            ? <>
              <h3>{name} - {url}</h3>
            </>
            : <>
              <h3>{url}</h3>
            </>
          }
          <span>Click on any element to copy to clipboard</span> <br />
          {email !== ''
            && <div onClick={e => copyToClip(e, email)}>
              <b>Email:</b> {email}<br />
              </div>
          }
          {username !== ''
            && <div onClick={e => copyToClip(e, username)}>
              <b>Username:</b> {username}<br />
              </div>
          }
          {password !== ''
            && <div onClick={e => copyToClip(e, password)}>
              <b>Password:</b> {'*'.repeat(password.length)}<br />
              </div>
          }
          <b>Description:</b> <br />
          <textarea rows={3} value={description} readOnly></textarea>
          <br />
          <ButtonGroup className="mt-4">
            <Button variant="secondary" onClick={e => edit()}>Edit</Button>
            <Button variant="danger" onClick={deleteCred}>Delete</Button>
          </ButtonGroup>
        </>
        : <>
          <Form onSubmit={save} className="mw-md-50 mw-xl-25">
            <Form.Group>
              <Form.Label>Name:</Form.Label>
              <Form.Control
                type="text"
                value={name}
                onChange={e => setName(e.currentTarget.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Website:</Form.Label>
              <Form.Control
                type="text"
                value={url}
                onChange={e => setUrl(e.currentTarget.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email:</Form.Label>
              <Form.Control
                type="text"
                value={email}
                onChange={e => setEmail(e.currentTarget.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Username:</Form.Label>
              <Form.Control
                type="text"
                value={username}
                onChange={e => setUsername(e.currentTarget.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Password:</Form.Label>
              <Form.Control
                type="text"
                value={password}
                onChange={e => setPassword(e.currentTarget.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Description:</Form.Label>
              <Form.Control
                type="text"
                value={description}
                onChange={e => setDescription(e.currentTarget.value)}
              />
            </Form.Group>
            <ButtonGroup className="mt-4">
              <Button variant="danger" onClick={cancel}>Cancel</Button>
              <Button variant="secondary" type="submit">Save</Button>
            </ButtonGroup>
          </Form>
        </>
      }
    </>
  );
};
