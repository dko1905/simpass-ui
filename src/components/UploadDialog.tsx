import {ChangeEvent, FC, useRef, useState} from 'react';
import { Button } from 'react-bootstrap';

interface Props {
  onCancel: () => void,
  onUpload: (text: string) => void,
};

export const UploadDialog: FC<Props> = (props) => {
  const [idPrefix, setIdPrefix] = useState(String(Math.round(Math.random() * 100)));
  const ignoreOuter = useRef(false);

  const onOuterClick = () => {
    console.log('Outer: ' + ignoreOuter.current);
    if (!ignoreOuter.current) props.onCancel();
    ignoreOuter.current = false;
  };
  const onInnerClick = () => {
    console.log('Inner: ' + ignoreOuter.current);
    ignoreOuter.current = true;
  };
  const onFileChange = async (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files == null || e.target.files.length < 1) return;
    const file = e.target.files[0];
    const text = await file.text()
    props.onUpload(text);
  };

  return (
    <div className="uploadDialog" onClick={onOuterClick}>
      <div className="dialog" onClick={onInnerClick}>
        <input
          type="file"
          accept="application/json"
          id={idPrefix + "-uploadDialog"}
          onChange={onFileChange}
        />
        <label htmlFor={idPrefix + "-uploadDialog"}>
          <Button as="span">Upload</Button>
        </label>
      </div>
    </div>
  );
};
