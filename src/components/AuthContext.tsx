import { createContext, FC, useContext, useEffect, useState } from 'react';
import { api } from '../api';

interface Props {
  children: JSX.Element[] | JSX.Element
};

const AuthContext = createContext<{
  userId: number,
  username: string,
  password: string,
  pin: string,
  saveLogin: boolean,
  isLoggedIn: boolean,
  isLoading: boolean,
  isDoneLoadingFromStorage: boolean,
  login: (username: string, password: string, pin: string, saveLogin: boolean) => Promise<void>,
  getLoginInfo: () => Promise<{username: string, password: string, pin: string, saveLogin: boolean}>,
  logout: () => Promise<void>
} | null>(null);

export const AuthProvider: FC<Props> = (props) => {
  const [userId, setUserId] = useState(0);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [pin, setPin] = useState('');
  const [saveLogin, setSaveLogin] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isDoneLoadingFromStorage, setIsDoneLoadingFromStorage] = useState(false);

  const login = async (username: string, password: string, pin: string, saveLogin: boolean) => {
    setIsLoading(true);
    try {
      // Login succeeded
      const body = await api.auth.authenticate(username, password);
      setUserId(body.id);
      setUsername(username);
      setPassword(password);
      setPin(pin);
      setSaveLogin(saveLogin);

      if (saveLogin && localStorage != null) {
        const txt = JSON.stringify({
          username, password, pin, saveLogin
        });
        console.log('Saving login info to LocalStorage');
        localStorage.setItem('login-information', txt);
      }

      setIsLoading(false);
      setIsLoggedIn(true);
    } catch (err) {
      setIsLoading(false);
      throw err;
    }
  };
  
  const getLoginInfo = async () => {
    return {username, password, pin, saveLogin};
  };

  const logout = async () => {
    // TODO: Spring security needs to remove cookies
    setUserId(0);
    setUsername('');
    setPassword('');
    setPin('');
    setSaveLogin(false);

    setIsLoading(true);

    if (localStorage != null) {
      localStorage.setItem('login-information', '');
    }
    await api.auth.logout();

    setIsLoading(false);
    setIsLoggedIn(false);
  }

  useEffect(() => {
    const txt = localStorage?.getItem('login-information');
    if (txt == null || txt === '') {
      console.debug('No login saved in localStorage');
      return;
    }
    console.debug('Login saved in localStorage');
    let data: { username: string, password: string, pin: string, saveLogin: boolean } | null = null;
    try {
      data = JSON.parse(txt);
    } catch (err) {
      console.warn('Invalid saved login information');
      console.warn(err);
      if (err instanceof Error) console.warn(err?.stack);
      localStorage.setItem('login-information', ''); // Fix current state
      return;
    }
    if (data === null || data.username === '' || data.password === '' || data.pin === '') {
      console.warn('Invalid saved login information');
      localStorage.setItem('login-information', ''); // Fix current state
      return;
    }

    setUsername(data.username);
    setPassword(data.password);
    setPin(data.pin);
    setSaveLogin(data.saveLogin);
    setIsDoneLoadingFromStorage(true);
  }, []);

  const state = {
    userId,
    username,
    password,
    pin,
    saveLogin,
    isLoggedIn,
    isLoading,
    isDoneLoadingFromStorage,
    login,
    getLoginInfo,
    logout
  };

  return (
    <AuthContext.Provider value={state}>
      {props.children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext)!;
